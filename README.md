I'm in process of moving from GitHub. You can find my other work [over there](https://github.com/AverageHelper) in the meantime.

I also mirror some projects to [Codeberg](https://codeberg.org/AverageHelper).
